const fs = require('fs');
const fetch = require('node-fetch');

fs.readFile('catalog.json', 'utf8', function (err, data) {
    if (err) throw err;
    let catalog = JSON.parse(data);

    for (var sections=0; sections < catalog.sectionList.length; sections++) {
        var sessions=catalog.sectionList[sections].items;
        for (var key in sessions) {
            var value = sessions[key];
            if (value.files !== undefined) {
                download(value.codeParts.alpha0, value.files[0].filename, value.files[0].url)
                .then((filename) => {console.log("downloaded: " + filename)});
            }   
        }
    }
});

async function download(track, filename, url) {
    
    //console.log(track + " - "+ filename);
    const dir = "./" + track;
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
    try {
        const res = await fetch(url, {            
            method: "GET"
        });

        return await new Promise((resolve, reject) => {
            const fileStream = fs.createWriteStream(dir + "/" + filename);
            res.body.pipe(fileStream);
            res.body.on("error", (err) => {
                reject(err);
            });
            fileStream.on("finish", function() {
                resolve(filename);
            });
        });            
    } catch (err) {
        console.log("error: " + url);
    }
}
