# SAPTechEd2021PDFDownloader

SAP TechEd 2021 session material (PDF) downloader

# Preparations

For the downloader to work, the session catalog is needed. You'll have to download it from the SAP TechEd event site:

1. Open the SAP TechEd session catalog: https://reg.sapevents.sap.com/flow/sap/sapteched2021/portal/page/sessions

2. Open the Developer Console of your browser, search for search (thats the API endoint): https://events.rainfocus.com/api/search

3. Search contains the response with the entire session catalog. In my case: 552KB zipped.

4. Download the catalog as a file. Right click on search and chose the option of your choice, like cURL.

5. Download the catalog via e.g. cURL (provide -o catalog.json to download the content to disk)

# Run

Follow the preparations and you should have a file named catalog.json. Check out this repo and place the catalog.json file in the root (next to index.js). Now install the dependencies and run the downloader. That's it.

```sh
npm install
npm start
```

Download will start, based on the files found in the catalog.json file. Download is asynchronous and should not take too long, maybe 2 or 3 minutes, if at all.

Output:

```sh
downloaded: DT111_approved.pdf
downloaded: DT102_e1_d3_approved.pdf
downloaded: DEV110_E1_d1.pdf
downloaded: ISP101_e2_d3.pdf
downloaded: DEV100_E1_d1_approved.pdf
downloaded: ANA206_e2_d2_approved.pdf
downloaded: DEV108_E1_d1_approved.pdf
```

Note: not all sessions have a file associated with it. Either because it was a discussion, or maybe the presentation is not (yet?) released. You should get 100+ session, out of a catalog of 220+ session.
